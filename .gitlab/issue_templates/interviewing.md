## Screening/Interview Training 

Before engaging candidates in a screening call or interview, please complete all of the following training. 

1. [ ] Watch training video on [conducting an interview at GitLab](https://www.youtube.com/watch?v=TKWxUeDI-9A)
1. [ ] Complete all modules of [Salesforce Equality at Work Training](https://about.gitlab.com/culture/inclusion/resources/#salesforce-inclusion-training).  To earn badges and save your responses, you'll need to sign up! Use your GitLab address to sign in using Google+.  
   1. [ ]  Once you've completed the units, from your Salesforce Dashboard, go to the "profile" tab, take a screenshot of your earned badges, and add that screenshot to the comments at bottom of this issue. 
1. [ ] Watch [STAR interviewing training video](https://www.youtube.com/watch?v=wlTRusmpyGk)
1. [ ] How to use Lever: [Video - Lever team](https://drive.google.com/a/gitlab.com/file/d/0B78MHKvas4zmb1FCak5vXy1ZVGs/view?usp=sharing )
1. [ ] [Read interview process in the handbook](https://about.gitlab.com/handbook/hiring/interviewing/)
1. [ ] Schedule and complete two screening call shadows with recruiter


